#include <cstring>
#include <iostream>
#include <iterator>
#include "debug.cpp"
#include <vector>
struct Card {
  enum cardValue { nullValue=0, ace, two, three, four, five, six, seven, eight, nine, ten, jack, queen, king };
  enum cardSuit { nullSuit=0, hearts, clubs, spades, diamonds };


  cardValue mValue;
  cardSuit mSuit;
};

static void const printCard(Card card) {
  std::string cardStringValue;
  std::string cardStringSuit;
  switch(card.mValue) {
    case Card::nullValue : cardStringValue = "nullValue"; break;
    case Card::ace : cardStringValue = "ace"; break;
    case Card::two : cardStringValue = "two"; break;
    case Card::three : cardStringValue = "three"; break;
    case Card::four : cardStringValue = "four"; break;
    case Card::five : cardStringValue = "five"; break;
    case Card::six : cardStringValue = "six"; break;
    case Card::seven : cardStringValue = "seven"; break;
    case Card::eight : cardStringValue = "eight"; break;
    case Card::nine : cardStringValue = "nine"; break;
    case Card::ten : cardStringValue = "ten"; break;
    case Card::jack : cardStringValue = "jack"; break;
    case Card::queen : cardStringValue = "queen"; break;
    case Card::king : cardStringValue = "king"; break;
    default: cardStringValue = "shit"; break;
  }
  switch (card.mSuit) {
    case Card::nullSuit : cardStringSuit = "nullSuit"; break;
    case Card::hearts : cardStringSuit = "hearts"; break;
    case Card::clubs : cardStringSuit = "clubs"; break;
    case Card::spades : cardStringSuit = "spades"; break;
    case Card::diamonds : cardStringSuit = "diamonds"; break;
    default: cardStringSuit = "brick"; break;
  }
  std::cout << cardStringValue << " of " << cardStringSuit << std::endl;
};

static Card::cardValue nrToCardValue(unsigned short valueNr) {
  switch(valueNr) {
    case 0: return Card::nullValue; break;
    case 1: return Card::ace; break;
    case 2: return Card::two; break;
    case 3: return Card::three; break;
    case 4: return Card::four; break;
    case 5: return Card::five; break;
    case 6: return Card::six; break;
    case 7: return Card::seven; break;
    case 8: return Card::eight; break;
    case 9: return Card::nine; break;
    case 10: return Card::ten; break;
    case 11: return Card::jack; break;
    case 12: return Card::queen; break;
    case 13: return Card::king; break;
    default: return Card::nullValue; break;
  }
};

struct CardStack {
    enum cardVisibility { none = 0, top, all };

    cardVisibility mCardVisibility;
    std::vector<Card> mStack;

    CardStack(): mCardVisibility(none){};
    CardStack(cardVisibility aCardVisibility): mCardVisibility(aCardVisibility){};

};

void addCard(CardStack& stack, Card::cardValue value, Card::cardSuit suit) {
  Card card;
  stack.mStack.push_back(card);
  unsigned short topCard = stack.mStack.size()-1;
  stack.mStack[topCard].mValue = value;
  stack.mStack[topCard].mSuit = suit;
};

void newDeck(CardStack& stack) {

};
